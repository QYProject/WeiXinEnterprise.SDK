﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeiXinEnterprise.SDK.API.Department;
using WeiXinEnterprise.SDK.API.Menu;
using WeiXinEnterprise.SDK.API.Message;
using WeiXinEnterprise.SDK.API.Message.MessageType;
using WeiXinEnterprise.SDK.API.User;

namespace WeiXinEnterprise.SDK.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Token.SetKey("wx2f2874377db124e5", "q26FOzra1FEOVJ9TrKGyX7GRQI4-yTGxS_pE2VqhnzB9IUuM9L6u0ezNLcj2GUqA");
            Token.GetToken();
        }

        #region 消息测试

        private static void Message()
        {
            UserList userList = new UserList();
            userList.department_id = "1";
            userList.fetch_child = "1";
            userList.status = "0";
            UserListResult listresult = userList.Send();

            var user = listresult.userlist.Where(u => u.name == "简健明").FirstOrDefault();

            MessageTextSend textSend = new MessageTextSend();
            textSend.touser = user.userid;
            textSend.agentid = "3";
            textSend.text = new MessageTextSend.MessageText() { content = "测试消息" };
            textSend.Send();

            MessageNewsSend newsSend = new MessageNewsSend();
            newsSend.touser = user.userid;
            newsSend.agentid = "3";
            newsSend.news.articles = new MessageNewsSend.MessageNews.MessageNewsItem[] {
                new MessageNewsSend.MessageNews.MessageNewsItem { title ="应用测试标题", description = "应用测试描述", url="https://www.baidu.com/" }
            };
            MessageSendRequest result = newsSend.Send();
        }

        #endregion

        #region 管理部门接口测试

        private static void DepmentTest()
        {
            DepartmentCreate depCreate = new DepartmentCreate();
            depCreate.name = "测试部门";
            depCreate.parentid = "1";
            depCreate.Send();

            string id = "";

            DepartmentList depList = new DepartmentList();
            DepartmentListResult resultList = depList.Send();
            foreach (var dep in resultList.department)
            {
                if (dep.name == "测试部门")
                {
                    id = dep.id;
                }
            }

            DepartmentUpdate depUpdate = new DepartmentUpdate();
            depUpdate.id = id;
            depUpdate.name = "Test";
            depUpdate.Send();

            DepartmentDelete depDelete = new DepartmentDelete();
            depDelete.id = id;
            OperationResultsBase result = depDelete.Send();
        }

        #endregion
    }
}
