﻿namespace WeiXinEnterprise.SDK.API.Message.MessageType
{
    public class MessageNewsSend : MessageSend
    {
        public override string msgtype { get; set; } = "news";

        /// <summary>
        /// 图文消息，一个图文消息支持1到8条图文
        /// </summary>
        public MessageNews news { get; set; } = new MessageNews();

        public class MessageNews
        {
            public MessageNewsItem[] articles { get; set; }

            public class MessageNewsItem
            {
                /// <summary>
                /// 标题，不超过128个字节，超过会自动截断
                /// </summary>
                public string title { get; set; }

                /// <summary>
                /// 描述，不超过512个字节，超过会自动截断
                /// </summary>
                public string description { get; set; }

                /// <summary>
                /// 点击后跳转的链接。
                /// </summary>
                public string url { get; set; }

                /// <summary>
                /// 图文消息的图片链接，支持JPG、PNG格式，较好的效果为大图640*320，小图80*80。如不填，在客户端不显示图片
                /// </summary>
                public string picurl { get; set; }
            }
        }
    }
}
