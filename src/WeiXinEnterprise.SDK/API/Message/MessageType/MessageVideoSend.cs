﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeiXinEnterprise.SDK.API.Message.MessageType
{
    public class MessageVideoSend : MessageSend
    {
        public override string msgtype { get; set; } = "video";

        /// <summary>
        /// 表示是否是保密消息，0表示否，1表示是，默认0
        /// </summary>
        public string safe { get; set; }

        [IsNotNull]
        public MessageVideo video { get; set; }

        public class MessageVideo
        {
            /// <summary>
            /// 语音文件id，可以调用上传临时素材或者永久素材接口获取
            /// </summary>
            [IsNotNull]
            public string media_id { get; set; }

            /// <summary>
            /// 视频消息的标题，不超过128个字节，超过会自动截断
            /// </summary>
            public string title { get; set; }

            /// <summary>
            /// 视频消息的描述，不超过512个字节，超过会自动截断
            /// </summary>
            public string description { get; set; }
        }
    }
}
