﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeiXinEnterprise.SDK.API.Message.MessageType
{
    /// <summary>
    /// text消息
    /// </summary>
    public class MessageTextSend : MessageSend
    {
        public override string msgtype { get; set; } = "text";

        /// <summary>
        /// 表示是否是保密消息，0表示否，1表示是，默认0
        /// </summary>
        public string safe { get; set; }

        [IsNotNull]
        public MessageText text { get; set; }

        public class MessageText
        {
            /// <summary>
            /// 消息内容，最长不超过2048个字节，注意：主页型应用推送的文本消息在微信端最多只显示20个字（包含中英文）
            /// </summary>
            [Length(1, 1024)]
            [IsNotNull]
            public string content { get; set; }
        }
    }
}
