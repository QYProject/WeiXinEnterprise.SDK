﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeiXinEnterprise.SDK.API.Message.MessageType
{
    /// <summary>
    /// image消息
    /// </summary>
    public class MessageImageSend : MessageSend
    {
        public override string msgtype { get; set; } = "image";

        /// <summary>
        /// 表示是否是保密消息，0表示否，1表示是，默认0
        /// </summary>
        public string safe { get; set; }

        [IsNotNull]
        public MessageImage image { get; set; }

        public class MessageImage
        {
            /// <summary>
            /// 图片媒体文件id，可以调用上传临时素材或者永久素材接口获取,永久素材media_id必须由发消息的应用创建
            /// </summary>
            [IsNotNull]
            public string media_id { get; set; }
        }
    }
}
