﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeiXinEnterprise.SDK.API.Message.MessageType
{
    public class MessageVoiceSend : MessageSend
    {
        public override string msgtype { get; set; } = "voice";

        /// <summary>
        /// 表示是否是保密消息，0表示否，1表示是，默认0
        /// </summary>
        public string safe { get; set; }

        [IsNotNull]
        public MessageVoice voice { get; set; }

        public class MessageVoice
        {
            /// <summary>
            /// 语音文件id，可以调用上传临时素材或者永久素材接口获取
            /// </summary>
            [IsNotNull]
            public string media_id { get; set; }
        }
    }
}
