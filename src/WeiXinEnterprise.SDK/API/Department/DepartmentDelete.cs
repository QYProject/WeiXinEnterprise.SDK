﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeiXinEnterprise.SDK.Request;

namespace WeiXinEnterprise.SDK.API.Department
{
    /// <summary>
    /// 删除指定部门
    /// </summary>
    public class DepartmentDelete : OperationRequestBase<OperationResultsBase, HttpGetRequest>
    {
        protected override string Url()
        {
            return "https://qyapi.weixin.qq.com/cgi-bin/department/delete?access_token=ACCESS_TOKEN&id="+ id;
        }

        /// <summary>
        /// 部门id。（注：不能删除根部门；不能删除含有子部门、成员的部门）
        /// </summary>
        [IsNotNull]
        public string id { get; set; }
    }
}
