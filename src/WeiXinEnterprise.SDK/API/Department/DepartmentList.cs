﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeiXinEnterprise.SDK.Request;

namespace WeiXinEnterprise.SDK.API.Department
{
    /// <summary>
    /// 获取部门列表
    /// </summary>
    public class DepartmentList : OperationRequestBase<DepartmentListResult, HttpGetRequest>
    {
        protected override string Url()
        {
            if(string.IsNullOrEmpty(id))
            {
                return "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token=ACCESS_TOKEN&id=" + id;
            }
            return "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token=ACCESS_TOKEN";
        }

        /// <summary>
        /// 部门id。获取指定部门及其下的子部门
        /// </summary>
        public string id { get; set; }
    }

    public class DepartmentListResult : OperationResultsBase
    {
        /// <summary>
        /// 部门列表数据。以部门的order字段从小到大排列
        /// </summary>
        public List<DepartmentItem> department { get; set; }

        public class DepartmentItem
        {
            /// <summary>
            /// 部门id
            /// </summary>
            public string id { get; set; }

            /// <summary>
            /// 部门名称
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 父亲部门id。根部门为1
            /// </summary>
            public string parentid { get; set; }

            /// <summary>
            /// 在父部门中的次序值。order值小的排序靠前。
            /// </summary>
            public string order { get; set; }
        }
    }
}
