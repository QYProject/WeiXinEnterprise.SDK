﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeiXinEnterprise.SDK.Request;

namespace WeiXinEnterprise.SDK.API.Department
{
    /// <summary>
    /// 部门信息更新
    /// </summary>
    public class DepartmentUpdate : OperationRequestBase<OperationResultsBase, HttpPostRequest>
    {
        protected override string Url()
        {
            return "https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token=ACCESS_TOKEN";
        }

        /// <summary>
        /// 部门id
        /// </summary>
        [IsNotNull]
        public string id { get; set; }

        /// <summary>
        /// 更新的部门名称。长度限制为32个字（汉字或英文字母），字符不能包括\:*?"<>｜。修改部门名称时指定该参数
        /// </summary>
        [Length(1, 32)]
        [IsNotNull]
        public string name { get; set; }

        /// <summary>
        /// 父亲部门id。根部门id为1
        /// </summary>
        public string parentid { get; set; }

        /// <summary>
        /// 在父部门中的次序值。order值小的排序靠前。
        /// </summary>
        public string order { get; set; }
    }
}
