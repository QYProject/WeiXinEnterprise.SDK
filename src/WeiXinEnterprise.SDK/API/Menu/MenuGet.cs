﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeiXinEnterprise.SDK.Request;

namespace WeiXinEnterprise.SDK.API.Menu
{
    public class MenuGet : OperationRequestBase<MenuGetResult, HttpGetRequest>
    {
        protected override string Url()
        {
            return "https://qyapi.weixin.qq.com/cgi-bin/menu/get?access_token=ACCESS_TOKEN&agentid=";
        }

        /// <summary>
        /// 企业应用的id，整型。可在应用的设置页面查看
        /// </summary>
        public string agentid { get; set; }
    }

    public class MenuGetResult : OperationResultsBase
    {
        public MenuButton button { get; set; }
    }
}
