﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeiXinEnterprise.SDK.Request;

namespace WeiXinEnterprise.SDK.API.Menu
{
    public class MenuCreate : OperationRequestBase<OperationResultsBase, HttpPostRequest>
    {
        protected override string Url()
        {
            return "https://qyapi.weixin.qq.com/cgi-bin/menu/create?access_token=ACCESS_TOKEN&agentid=" + agentid;
        }

        /// <summary>
        /// 企业应用的id，整型。可在应用的设置页面查看
        /// </summary>
        public string agentid { get; set; }

        /// <summary>
        /// 一级菜单数组，个数应为1~3个
        /// </summary>
        public MenuButton button { get; set; } = new MenuButton();
    }
}
