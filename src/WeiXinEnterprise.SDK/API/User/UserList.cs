﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeiXinEnterprise.SDK.Request;

namespace WeiXinEnterprise.SDK.API.User
{
    public class UserList : OperationRequestBase<UserListResult, HttpGetRequest>
    {
        protected override string Url()
        {
            return string.Format("https://qyapi.weixin.qq.com/cgi-bin/user/list?access_token=ACCESS_TOKEN&department_id={0}&fetch_child={1}&status={2}", department_id, fetch_child, status);
        }

        /// <summary>
        /// 获取的部门id
        /// </summary>
        [IsNotNull]
        public string department_id { get; set; }

        /// <summary>
        /// 1/0：是否递归获取子部门下面的成员
        /// </summary>
        public string fetch_child { get; set; }

        /// <summary>
        /// 0获取全部成员，1获取已关注成员列表，2获取禁用成员列表，4获取未关注成员列表。status可叠加，未填写则默认为4
        /// </summary>
        public string status { get; set; } = "4";
    }

    public class UserListResult : OperationResultsBase
    {
        /// <summary>
        /// 成员列表
        /// </summary>
        public List<UserListItem> userlist { get; set; }

        public class UserListItem
        {
            /// <summary>
            /// 成员UserID。对应管理端的帐号
            /// </summary>
            public string userid { get; set; }

            /// <summary>
            /// 成员名称
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 成员所属部门id列表
            /// </summary>
            public string[] department { get; set; }

            /// <summary>
            /// 职位信息
            /// </summary>
            public string position { get; set; }

            /// <summary>
            /// 手机号码
            /// </summary>
            public string mobile { get; set; }

            /// <summary>
            /// 性别。0表示未定义，1表示男性，2表示女性
            /// </summary>
            public string gender { get; set; }

            /// <summary>
            /// 邮箱
            /// </summary>
            public string email { get; set; }

            /// <summary>
            /// 微信号
            /// </summary>
            public string weixinid { get; set; }

            /// <summary>
            /// 头像url。注：如果要获取小图将url最后的"/0"改成"/64"即可
            /// </summary>
            public string avatar { get; set; }

            /// <summary>
            /// 关注状态: 1=已关注，2=已冻结，4=未关注
            /// </summary>
            public string status { get; set; }

            /// <summary>
            /// 扩展属性
            /// </summary>
            public UserListExtAttr extattr { get; set; }
        }

        public class UserListExtAttr
        {
            public List<Attr> attrs { get; set; }

            public class Attr
            {
                public string name { get; set; }

                public string value { get; set; }
            }
        }
    }
}
