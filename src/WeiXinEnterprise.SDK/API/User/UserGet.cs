﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeiXinEnterprise.SDK.Request;

namespace WeiXinEnterprise.SDK.API.User
{
    public class UserGet : OperationRequestBase<UserGetResult, HttpGetRequest>
    {
        protected override string Url()
        {
            return "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&userid="+ userid;
        }

        /// <summary>
        /// 成员UserID。对应管理端的帐号
        /// </summary>
        [IsNotNull]
        public string userid { get; set; }
    }

    public class UserGetResult : OperationResultsBase
    {
        /// <summary>
        /// 成员UserID。对应管理端的帐号
        /// </summary>
        public string userid { get; set; }

        /// <summary>
        /// 成员名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 成员所属部门id列表
        /// </summary>
        public string[] department { get; set; }

        /// <summary>
        /// 职位信息
        /// </summary>
        public string position { get; set; }

        /// <summary>
        /// 手机号码
        /// </summary>
        public string mobile { get; set; }

        /// <summary>
        /// 性别。0表示未定义，1表示男性，2表示女性
        /// </summary>
        public string gender { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        public string email { get; set; }

        /// <summary>
        /// 微信号
        /// </summary>
        public string weixinid { get; set; }

        /// <summary>
        /// 头像url。注：如果要获取小图将url最后的"/0"改成"/64"即可
        /// </summary>
        public string avatar { get; set; }

        /// <summary>
        /// 关注状态: 1=已关注，2=已禁用，4=未关注
        /// </summary>
        public string status { get; set; }

        /// <summary>
        /// 扩展属性
        /// </summary>
        public UserGetExtAttr extattr { get; set; }
    }

    public class UserGetExtAttr
    {
        public List<Attr> attrs { get; set; }

        public class Attr
        {
            public string name { get; set; }

            public string value { get; set; }
        }
    }
}
