﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeiXinEnterprise.SDK.Request;

namespace WeiXinEnterprise.SDK.API.User
{
    public class UserBatchDelete : OperationRequestBase<OperationResultsBase, HttpPostRequest>
    {
        protected override string Url()
        {
            return "https://qyapi.weixin.qq.com/cgi-bin/user/batchdelete?access_token=ACCESS_TOKEN";
        }

        /// <summary>
        /// 成员UserID列表。对应管理端的帐号。（最多支持200个）
        /// </summary>
        [IsNotNull]
        public List<string> useridlist { get; set; }
    }
}
