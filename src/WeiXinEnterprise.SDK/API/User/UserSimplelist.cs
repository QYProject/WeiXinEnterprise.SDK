﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeiXinEnterprise.SDK.Request;

namespace WeiXinEnterprise.SDK.API.User
{
    public class UserSimplelist : OperationRequestBase<UserSimplelistResult, HttpGetRequest>
    {
        protected override string Url()
        {
            return string.Format("https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token=ACCESS_TOKEN&department_id={0}&fetch_child={1}&status={2}", department_id, fetch_child, status);
        }

        /// <summary>
        /// 获取的部门id
        /// </summary>
        [IsNotNull]
        public string department_id { get; set; }

        /// <summary>
        /// 1/0：是否递归获取子部门下面的成员
        /// </summary>
        public string fetch_child { get; set; }

        /// <summary>
        /// 0获取全部成员，1获取已关注成员列表，2获取禁用成员列表，4获取未关注成员列表。status可叠加，未填写则默认为4
        /// </summary>
        public string status { get; set; } = "4";
    }

    public class UserSimplelistResult : OperationResultsBase
    {
        /// <summary>
        /// 成员列表
        /// </summary>
        public List<UserSimpleItem> userlist { get; set; }
        
        public class UserSimpleItem
        {
            /// <summary>
            /// 成员UserID。对应管理端的帐号
            /// </summary>
            public string userid { get; set; }

            /// <summary>
            /// 成员名称
            /// </summary>
            public string name { get; set; }

            /// <summary>
            /// 成员所属部门
            /// </summary>
            public string[] department { get; set; }
        }
    }
}
