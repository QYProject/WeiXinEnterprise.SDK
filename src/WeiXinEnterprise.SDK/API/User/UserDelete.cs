﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WeiXinEnterprise.SDK.Request;

namespace WeiXinEnterprise.SDK.API.User
{
    public class UserDelete : OperationRequestBase<OperationResultsBase, HttpGetRequest>
    {
        protected override string Url()
        {
            return "https://qyapi.weixin.qq.com/cgi-bin/user/delete?access_token=ACCESS_TOKEN&userid="+ userid;
        }

        /// <summary>
        /// 成员UserID。对应管理端的帐号
        /// </summary>
        [IsNotNull]
        public string userid { get; set; }
    }
}
