﻿namespace WeiXinEnterprise.SDK.Interface
{
    public interface ISend<out T>
        where T : OperationResultsBase, new()
    {
        T Send();
    }
}
