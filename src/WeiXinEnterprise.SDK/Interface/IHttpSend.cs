﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeiXinEnterprise.SDK.Interface
{
    public interface IHttpSend
    {
        string Send(string url, string data);
    }
}
