﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WeiXinEnterprise.SDK
{
    public class OperationResultsBase
    {
        public int errcode { get; set; }
        public string errmsg { get; set; }
    }
}
